import type { User } from "../types/type";

const authToken = "overflow_auth_token";
const userToken = "overflow_auth_user";

class Auth {
  saveLogin(token: string) {
    localStorage.setItem(authToken, token);
  }
  isLogging() {
    const token = localStorage.getItem(authToken) || "";

    return token.length > 0;
  }
  getUser() {
    try {
      JSON.parse(localStorage.getItem(userToken) || "null");
    } catch {
      return null;
    }
  }
  saveUser(user: User) {
    localStorage.setItem(userToken, JSON.stringify(user));
  }
  getToken() {
    return localStorage.getItem(authToken);
  }
}

export default new Auth();
