import type { AxiosError, AxiosInstance, AxiosResponse } from "axios";
import axios from "axios";
import auth from "./tokenServices";

export const basUrl = "/";

export const http: AxiosInstance = axios.create({ baseURL: basUrl });

export default {
  install(app: any) {
    const redirectTo = (path: string) => {
      app.config.globalProperties.$router.push(path);
    };

    const handleSuccess = (response: AxiosResponse): AxiosResponse => {
      return response;
    };

    const handleError = (error: AxiosError) => {
      switch (error?.response?.status) {
        case 401:
          redirectTo("/login");
          break;
        case 404:
          redirectTo("/not-found");
          break;
        default:
          break;
      }
    };

    http.interceptors.response.use(handleSuccess, handleError);
    http.interceptors.request.use((config) => {
      const token = auth.getToken();

      if (token && config && config.headers) {
        config.headers["Authorization"] = `Bearer ${token}`;
      }
      return config;
    });

    app.config.globalProperties.$http = http;
  },
};
