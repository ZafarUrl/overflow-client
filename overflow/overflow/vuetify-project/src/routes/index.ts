import type { RouteRecordRaw } from "vue-router";
import { createRouter, createWebHistory } from "vue-router";
import DashboardVue from "../views/Dashboard.vue";
import LoginVue from "../views/Login.vue";
import auth from "../utils/tokenServices";

const routes: Array<RouteRecordRaw> = [
  {
    name: "home",
    path: "/",
    component: DashboardVue,
    meta: { auth: true },
    // children: [],
  },
  {
    path: "/login",
    name: "login",
    component: LoginVue,
    meta: { auth: false },
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, form, next) => {
  let hasAccess = to?.meta?.auth;
  if (hasAccess) {
    next({ name: "login" });
  }
  next();
});

export default router;
