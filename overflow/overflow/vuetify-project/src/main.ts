import { createApp } from "vue";
import "./style.css";
import "./assets/style/main.scss";
import App from "./App.vue";
import router from "./routes";
import http from "./utils/http";
import baseButton from "./components/button/button.vue";
import { VueQueryPlugin } from "@tanstack/vue-query";

createApp(App)
  .use(router)
  .use(http)
  .use(VueQueryPlugin)
  .component("base-button", baseButton)
  .mount("#app");
