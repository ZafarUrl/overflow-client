interface UseGetParams {
  link: string;
  params?: Record<string, any>;
}

export const getParams = (
  data: Record<string, any> = {}
): UseGetParams["params"] => {
  const params: UseGetParams["params"] = {};
  for (const [key, value] of Object.entries(data)) {
    if (value && typeof value == "object" && "value" in value) {
      params[key] = value.value;
    }
  }
  return params;
};
