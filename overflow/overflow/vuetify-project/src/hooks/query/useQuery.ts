import { useQuery, useMutation } from "@tanstack/vue-query";
import { http } from "../../utils/http";
import { getParams } from "./getParams";

interface useGetParams {
  link: string;
  params?: Record<string, any>;
}

export interface useMutateOption {
  method?: "get" | "post" | "put" | "delete";
  link: string;
  data?: any;
  params?: Record<string, any>;
}

export const useGet = ({ link, params, ...options }: useGetParams) => {
  return useQuery({
    queryKey: [link, params],
    queryFn: () => http.get(link, { params: { ...getParams(params) } }),
    retry: 1,
  });
};

export const useMutate = ({ ...option }) =>
  useMutation({
    mutationFn: ({
      method = "post",
      link,
      data = null,
      params,
    }: useMutateOption) => http[method](link, data, { params }),
    retry: 1,
  });
